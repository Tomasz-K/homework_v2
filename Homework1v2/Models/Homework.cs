﻿using System.Collections.Generic;

namespace Homework1v2.Models
{
    public class Homework
    {
        public int MaxPoints { get; set; }
        public List<HomeworkResult> ResultsList { get; set; }
    }
}
