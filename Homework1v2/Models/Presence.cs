﻿namespace Homework1v2.Models
{
    public class Presence
    {
        public Student Student { get; set; }
        public bool IsPresent { get; set; }
    }
}
