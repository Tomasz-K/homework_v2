﻿using System;

namespace Homework1v2.Models
{
    public class Student
    {
        public int ID { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime Birthday { get; private set; }
        public string Gender { get; private set; }

        public Student(int Id, string firstName, string lastName, DateTime birthday, string gender)
        {
            ID = Id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Gender = gender;
        }
    }
}
