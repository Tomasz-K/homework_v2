﻿using Homework1v2.Models;
using System;
using System.Collections.Generic;

namespace Homework1v2.Models
{
    public class CourseRegister
    {
        public string CourseName { get;  set; }
        public DateTime CourseStartDate { get;  set; }
        public double HomeworkMargin { get;  set; }
        public double AttendanceMargin { get;  set; }

        public List<Student> StudentsList;
        public List<CourseDay> CourseDaysList = new List<CourseDay>();
        public List<Homework> HomeworksList = new List<Homework>();

        public Teacher Teacher { get; set; } 
    }
}
