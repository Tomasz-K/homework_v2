﻿namespace Homework1v2.Models
{
    public class HomeworkResult
    {
        public Student Student { get; set; }
        public double StudentResult { get; set; }
    }
}
