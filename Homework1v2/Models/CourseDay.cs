﻿using System.Collections.Generic;

namespace Homework1v2.Models
{
    public class CourseDay
    {
        public string Name { get; set; }
        public List<Presence> PresenceList { get; set; }
    }
}
