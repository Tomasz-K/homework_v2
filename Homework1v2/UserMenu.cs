﻿using Homework1v2.Services;
using System;
using System.Linq;

namespace Homework1v2
{
    public class UserMenu
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _inputReader = new InputReader();
        private string[] _options;

        public UserMenu(params string[] options)
        {
            _options = options.ToArray();
        }
                
        public void PrintUserMenu()
        {
            _consoleWriter.WriteMessage(" _______________________\n");
            _consoleWriter.WriteMessage("|     PRACA DOMOWA 1    |\n");
            _consoleWriter.WriteMessage("|       DZIENNIK        |\n");
            _consoleWriter.WriteMessage("|_______________________|\n");
            for (int i = 0; i < _options.Length; i++)
            {
                Console.WriteLine($"{i + 1}. {_options[i]}");
            }
        }

        public int AskUser()
        {
            _consoleWriter.WriteMessage("Wybierz numer menu od 1. do 6.: ");
            string input = _inputReader.ReadInput();
            int result;

            while(!int.TryParse(input, out result) || result <1 || result > _options.Length)
            {
                _consoleWriter.WriteMessage("Wybierz numer menu od 1. do 6.: ");
                input = _inputReader.ReadInput();
            }

            return result;
        }
    }
}
