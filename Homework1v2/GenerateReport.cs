﻿using Homework1v2.Data;
using Homework1v2.Models;
using Homework1v2.Services;
using System.Linq;

namespace Homework1v2
{
    public class GenerateReport
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _input = new InputReader();

        public void Generate()
        {
            double studentHomeworkResult;
            double studentAttendanceResult;

            double homeworkTotalPoints = 0; //wszystkie punkty z prac domowych

            double presenceTotalPoints = 0; //wszystkie punkty obecności

            PrintOutReport();

            foreach (var homework in ProgramData.CourseRegister.HomeworksList)
            {
                homeworkTotalPoints += homework.MaxPoints;
            }

            presenceTotalPoints = ProgramData.CourseRegister.CourseDaysList.Count();

            foreach (var student in ProgramData.CourseRegister.StudentsList)
            {
                studentHomeworkResult = CountHomeworkPoints(student); //wszystkie punkty studenta
                studentAttendanceResult = CountAttendancePoints(student); //wszystkie obecności studenta

                double homeworkPercentage = ((studentHomeworkResult * 1.0) / homeworkTotalPoints) * 100; // procent punktów z HW
                double presencePercentage = ((studentAttendanceResult * 1.0) / presenceTotalPoints) * 100; //procent obecności
                var homeworkResult = homeworkPercentage.ToString("F0");
                var presenceResult = presencePercentage.ToString("F0");

            
                _consoleWriter.WriteMessage($"\n{student.FirstName} {student.LastName}, {student.Gender.ToUpper()}, ur.{student.Birthday}\n" +
                    $"wynik z prac domowych: {studentHomeworkResult}/{homeworkTotalPoints} ({homeworkResult}%) - {CheckHomeworkGrade(homeworkPercentage)}\n" +
                    $"ilość obecności: {studentAttendanceResult}/{presenceTotalPoints} ({presenceResult}%) - {CheckPresenceGrade(presencePercentage)}\n");              
            }
            _consoleWriter.WriteMessage("Aby kontynuować naciśnij ENTER");
            _input.ReadKey();
        }

        private void PrintOutReport()
        {
            _consoleWriter.WriteMessage(" ------------------------------\n");
            _consoleWriter.WriteMessage("|        Raport z kursu        |\n");
            _consoleWriter.WriteMessage(" ------------------------------\n");
            _consoleWriter.WriteMessage($"Nazwa: {ProgramData.CourseRegister.CourseName}\n");
            _consoleWriter.WriteMessage($"Data rozpoczęcia: {ProgramData.CourseRegister.CourseStartDate}\n");
            _consoleWriter.WriteMessage($"Prowadzący: {ProgramData.CourseRegister.Teacher.FirstName} {ProgramData.CourseRegister.Teacher.LastName}\n");
            _consoleWriter.WriteMessage($"Próg zaliczenia obecności: {ProgramData.CourseRegister.AttendanceMargin}%\n");
            _consoleWriter.WriteMessage($"Próg zaliczenia pracy domowej: {ProgramData.CourseRegister.HomeworkMargin}%\n");
            _consoleWriter.WriteMessage("Lista studentów:\n");
        }

        private double CountHomeworkPoints(Student student)
        {
            double studentHomeworkPointsSum = 0;
            foreach (var homework in ProgramData.CourseRegister.HomeworksList)
            {
                foreach (var result in homework.ResultsList)
                {
                    if (result.Student == student)
                    {
                        studentHomeworkPointsSum += result.StudentResult;
                    }
                }
            }
            return studentHomeworkPointsSum;
        }

        private double CountAttendancePoints(Student student)
        {
            double studentPresencePoints = 0;
            foreach (var courseDay in ProgramData.CourseRegister.CourseDaysList)
            {
                foreach (var result in courseDay.PresenceList)
                {
                    if (result.Student == student && result.IsPresent == true)
                    {
                        studentPresencePoints++;
                    }
                }
            }
            return studentPresencePoints;
        }

        private string CheckHomeworkGrade(double homeworkPercentage)
        {
            string homeworkPassGrade;
            double passingPercent = ProgramData.CourseRegister.HomeworkMargin;

            homeworkPassGrade = CheckGrade(passingPercent, homeworkPercentage);
    
            return homeworkPassGrade;
        }

        private string CheckPresenceGrade(double presencePercentage)
        {
            string presencePassGrade;
            double passingPercent = ProgramData.CourseRegister.AttendanceMargin;

            presencePassGrade = CheckGrade(passingPercent, presencePercentage);

            return presencePassGrade;
        }

        private string CheckGrade(double passingPercent, double studentPercentage)
        {
            string passGrade;
            if (studentPercentage >= passingPercent)
            {
                passGrade = "Zal.";
            }
            else
            {
                passGrade = "Niezal.";
            }

            return passGrade;
        }
    }
}

