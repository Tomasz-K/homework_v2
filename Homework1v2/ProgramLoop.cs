﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework1v2.Models;
using Homework1v2.Services;
using Homework1v2.Data;

namespace Homework1v2
{
    public class ProgramLoop
    {
        //custom write * read from console
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _input = new InputReader();

        private static GenerateReport generateReport = new GenerateReport();                 

        private TeacherService _teacherService = new TeacherService();

        private ValidateInput _validateUserInput = new ValidateInput();

        private static CourseRegister _courseRegister;

        public ProgramLoop()
        {
            ProgramData.CourseRegister = new CourseRegister();
        }

        public void Run()
        {
            ChooseMenuNumber userChoice;

            do
            {
                var userMenu = new UserMenu("Dodaj nowy kurs", "Dodaj kursanta/ów", "Dodaj dzień zajęć", "Dodaj pracę domową", "Generuj raport z kursu", "Wyjście");
                userMenu.PrintUserMenu();

                userChoice = (ChooseMenuNumber)userMenu.AskUser();

                switch (userChoice)
                {
                    case ChooseMenuNumber.AddNewCourseRegister:
                        {
                            AddNewCourseRegister();
                     
                            break;
                        }
                    case ChooseMenuNumber.AddStudents:
                        {
                            AddStudents();        

                            break;
                        }
                    case ChooseMenuNumber.AddCourseDay:
                        {
                            AddCourseDay();

                            break;
                        }
                    case ChooseMenuNumber.AddHomework:
                        {
                            AddHomework();

                            break;
                        }
                    case ChooseMenuNumber.GenerateReport:
                        {
                            GenerateReport();

                            break;
                        }
                    case ChooseMenuNumber.Exit:
                        {                            
                            return;
                        }
                }
                _consoleWriter.ClearConsole();

            } while (userChoice != ChooseMenuNumber.Exit);
        }

        private void AddNewCourseRegister()
        {
            if (_courseRegister == null)
            {
                _courseRegister = new CourseRegister();
                CourseRegisterService _courseRegisterService = new CourseRegisterService();

                _courseRegister = _courseRegisterService.AddCourseRegister();
                var teacher = _teacherService.AddTeacher();
                _courseRegister.Teacher = teacher;

                ProgramData.CourseRegister = _courseRegister;
            }
            else
            {
                _consoleWriter.WriteMessage("Już stworzyłeś kurs!\n");
                PauseLoop();
            }
        }
       
        private void AddStudents()
        {
            if (_courseRegister == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw stworzyć kurs!");
                PauseLoop();
            }
            else if (ProgramData.CourseRegister.StudentsList != null)
            {
                _consoleWriter.WriteMessage("Już dodałeś studentów!");
                PauseLoop();
            }
            else
            {
                StudentService _studentService = new StudentService();
                _courseRegister.StudentsList = new List<Student>();

                int studentCount = _studentService.AddStudentCount();

                for (int i = 0; i < studentCount; i++)
                {
                    Console.WriteLine($"\nDodaj kursanta {i + 1}");

                    Student newStudent = _studentService.AddNewStudent(studentCount); //dodaj nowego studenta

                    _courseRegister.StudentsList.Add(newStudent);
          
                }
                _consoleWriter.WriteMessage("Dodano wszystkich studentów");
                ProgramData.CourseRegister.StudentsList = _courseRegister.StudentsList;
            }
        }
      
        private void AddCourseDay()
        {
            if (_courseRegister == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw stworzyć kurs!");
                PauseLoop();
            }
            else if (ProgramData.CourseRegister.StudentsList == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw dodać studentów!");
                PauseLoop();
            }
            else
            {
                CourseDayService _courseDayService = new CourseDayService();
                CourseDay day = _courseDayService.AddDay(ProgramData.CourseRegister.StudentsList);

                ProgramData.CourseRegister.CourseDaysList.Add(day);
            }
        }

        private void AddHomework()
        {
            if (_courseRegister == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw stworzyć kurs!");
                PauseLoop();
            }
            else if (ProgramData.CourseRegister.StudentsList == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw dodać studentów!");
                PauseLoop();
            }
            else
            {
                HomeworkService _homeworkService = new HomeworkService();
                Homework newHomework = new Homework();
                newHomework = _homeworkService.AddHomework(ProgramData.CourseRegister.StudentsList);

                ProgramData.CourseRegister.HomeworksList.Add(newHomework);
            }
        }
        
        private void GenerateReport()
        {
            if (_courseRegister == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw stworzyć kurs!");
                PauseLoop();
            }
            else if (_courseRegister.StudentsList == null)
            {
                _consoleWriter.WriteMessage("Musisz najpierw dodać studentów!");
                PauseLoop();
            }
            else
            {
                generateReport.Generate();
            }
        }
       
        private void PauseLoop()
        {
            System.Threading.Thread.Sleep(1000);
        }
    }
}