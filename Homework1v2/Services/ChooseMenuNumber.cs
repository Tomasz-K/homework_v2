﻿namespace Homework1v2.Services
{
    public enum ChooseMenuNumber
    {
        AddNewCourseRegister = 1,
        AddStudents = 2,
        AddCourseDay = 3,
        AddHomework = 4,
        GenerateReport = 5,
        Exit = 6
    }
}
