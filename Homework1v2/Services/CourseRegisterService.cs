﻿using Homework1v2.Models;

namespace Homework1v2.Services
{
    public class CourseRegisterService
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _userInput = new InputReader();
        private ValidateInput _validateUserInput = new ValidateInput();
        public CourseRegister _courseRegister = new CourseRegister();

        public CourseRegister AddCourseRegister()
        {
            _consoleWriter.WriteMessage("Podaj nazwę kursu: ");
            _courseRegister.CourseName = _userInput.ReadInput();

            _consoleWriter.WriteMessage("Podaj datę rozpoczęcia kursu (dd.mm.yyyy): ");
            _courseRegister.CourseStartDate = _validateUserInput.CheckDate();

            _consoleWriter.WriteMessage("Podaj próg zaliczenia obecności %: ");
            _courseRegister.AttendanceMargin = _validateUserInput.CheckPercent();
            
            _consoleWriter.WriteMessage("Podaj próg zaliczenia pracy domowej %: ");
            _courseRegister.HomeworkMargin = _validateUserInput.CheckPercent();          
                        
            return _courseRegister;
        }
    }
}