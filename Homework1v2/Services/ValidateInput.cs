﻿using System;
using System.Globalization;
using System.Linq;

namespace Homework1v2.Services
{
    public class ValidateInput
    {
        private InputReader _userInput = new InputReader();
        private ConsoleWriter _consoleWriter = new ConsoleWriter();

        public int CheckId()
        {
            string input = _userInput.ReadInput();
            int result;

            while (!(int.TryParse(input, out result)))
            {
                _consoleWriter.WriteMessage("Nieprawidłowy numer!\n");
                input = _userInput.ReadInput();
            }
            return result;
        }
        
        public string CheckFirstName()
        {
            string input = _userInput.ReadInput();
          
            while (String.IsNullOrEmpty(input) || !input.All(char.IsLetter))
            {
                _consoleWriter.WriteMessage("Musisz podać imię bez znaków specjalnych lub cyfr!\nPodaj jeszcze raz:");
                input = _userInput.ReadInput();
            }
  
            return input;
        }

        public string CheckLastName()
        {
            string input = _userInput.ReadInput();
           
            while (String.IsNullOrEmpty(input) || !input.All(char.IsLetter))
            {
                _consoleWriter.WriteMessage("Musisz podać nazwisko bez znaków specjalnych lub cyfr!\nPodaj jeszcze raz:");
                input = _userInput.ReadInput();
            }

            return input;
        }

        public DateTime CheckDate()
        {
            DateTime today = DateTime.Today;

            string input = _userInput.ReadInput();
            DateTime result = new DateTime();

            while (!(DateTime.TryParseExact(input, "d.M.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out result)))
            {
                _consoleWriter.WriteMessage("Błędna data. Podaj właściwy format (dd.mm.yyyy)\n");
                input = _userInput.ReadInput();
            }
            return result;
        }

        public int CheckPercent()
        {
            string input = _userInput.ReadInput();
            int result;

            while (!(int.TryParse(input, out result)) || result > 100 || result < 0)
            {
                _consoleWriter.WriteMessage("Nieprawidłowa liczba!\n");
                input = _userInput.ReadInput();
            }
            return result;
        }

        public int CheckCount()
        {
            string input = _userInput.ReadInput();
            int result;

            while (!int.TryParse(input, out result) || result == 0)
            {
                _consoleWriter.WriteMessage("Nieprawidłowa liczba!\n");
                input = _userInput.ReadInput();
            }
            return result;
        }

        public double CheckStudentResult()
        {
            string input = _userInput.ReadInput();
            double result;

            while (!double.TryParse(input, out result) || result < 0)
            {
                _consoleWriter.WriteMessage("Nieprawidłowy liczba!\n");
                input = _userInput.ReadInput();
            }
            return result;
        }

        public string CheckGender()
        {
            string input = _userInput.ReadInput();

            while (input.ToLower() != "m" && input.ToLower() != "k")
            {
                _consoleWriter.WriteMessage("Podaj tylko K lub M!\n");
                input = _userInput.ReadInput();
            }
            return input;
        }

        public bool ReadPresence()
        {
            string input = _userInput.ReadInput();
            int result;
            bool presence = false;

            while (!(int.TryParse(input, out result)) || (result != 1 && result != 2))
            {
                _consoleWriter.WriteMessage("Wciśnij 1 dla obecny, 2 dla nieobecny!\n");
                input = _userInput.ReadInput();
            }

            if (result == 1)
            {
                presence = true;
            }
            else if (result == 2)
            {
                presence = false;
            }
            return presence;
        }
    }
}
