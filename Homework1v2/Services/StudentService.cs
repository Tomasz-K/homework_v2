﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework1v2.Models;
using Homework1v2.Data;

namespace Homework1v2.Services
{
    public class StudentService
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _userInput = new InputReader();
        private ValidateInput _validateUserInput = new ValidateInput();

        public Student AddNewStudent(int studentCount)
        {
            _consoleWriter.WriteMessage("Podaj identyfikator kursanta: ");
            int Id = _validateUserInput.CheckId();

            CheckAgain:
            foreach (var Student in ProgramData.CourseRegister.StudentsList)
            {
                while (Student.ID == Id)
                {
                    _consoleWriter.WriteMessage("Student o podanym ID już istnieje, podaj ID jeszcze raz\n");
                    Id = _validateUserInput.CheckId();
                    goto CheckAgain;
                }
            }

            _consoleWriter.WriteMessage("Podaj imię kursanta: ");
            string firstName = _validateUserInput.CheckFirstName();

            _consoleWriter.WriteMessage("Podaj nazwisko kursanta: ");
            string lastName = _validateUserInput.CheckLastName();

            _consoleWriter.WriteMessage("Podaj datę urodzenia (dd.mm.yyyy): ");
            DateTime birthday = _validateUserInput.CheckDate();

            while (ProgramData.CourseRegister.CourseStartDate < birthday)
            {
                _consoleWriter.WriteMessage("O'Rly? Student urodzony po rozpoczęciu kursu? Podaj datę jeszce raz\n");
                birthday = _validateUserInput.CheckDate();
            }

            _consoleWriter.WriteMessage("Podaj płeć 'K'obieta / 'M'ężczyzna: ");
            string gender = _validateUserInput.CheckGender();

            Student student = new Student(Id, firstName, lastName, birthday, gender);

            _consoleWriter.WriteMessage("Dodano nowego kursanta!");

            return student;
        }

        public int AddStudentCount()
        {
            _consoleWriter.WriteMessage("Podaj liczbę kursantów: ");
            int studentCount = _validateUserInput.CheckCount();
            return studentCount;
        }
    }
}
