﻿using Homework1v2.Data;
using Homework1v2.Models;
using System.Collections.Generic;
namespace Homework1v2.Services
{
    public class CourseDayService
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _input = new InputReader();
        private ValidateInput _validateUserInput = new ValidateInput();

        public CourseDay AddDay(List<Student> studentsList)
        {
            CourseDay newDay = new CourseDay();

            newDay.PresenceList = new List<Presence>();

            _consoleWriter.WriteMessage("Podaj nazwę dnia: ");
            newDay.Name = _input.ReadInput();

            foreach (var student in ProgramData.CourseRegister.StudentsList)
            {
                Presence presence = new Presence();
                presence.Student= student;

                _consoleWriter.WriteMessage($"Czy {student.FirstName} {student.LastName} był obecny w {newDay.Name}\n1. tak 2. nie\n");
                bool present = _validateUserInput.ReadPresence();

                presence.IsPresent = present;

                newDay.PresenceList.Add(presence);
            }
            return newDay;
        }
        
    }
}
