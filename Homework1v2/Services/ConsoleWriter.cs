﻿using System;

namespace Homework1v2.Services
{
    public class ConsoleWriter
    {
        public void WriteMessage(string message)
        {
            Console.Write(message);
        }
        public void ClearConsole()
        {
            Console.Clear();
        }
    }
}
