﻿using Homework1v2.Models;

namespace Homework1v2.Services
{
    public class TeacherService
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();
        private InputReader _userInput = new InputReader();
        private ValidateInput _validateUserInput = new ValidateInput();

        public Teacher AddTeacher()
        {
            var _teacher = new Teacher();

            _consoleWriter.WriteMessage("Podaj imię nauczyciela: ");
            _teacher.FirstName = _validateUserInput.CheckFirstName();

            _consoleWriter.WriteMessage("Podaj nazwisko nauczyciela: ");
            _teacher.LastName = _validateUserInput.CheckLastName();

            return _teacher;
        }
    }
}
