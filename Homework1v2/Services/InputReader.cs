﻿using System;

namespace Homework1v2.Services
{
    public class InputReader
    {
        public string ReadInput()
        {
            string input = Console.ReadLine();
            return input;
        }

        public void ReadKey()
        {
            ConsoleKeyInfo cki;
            do {
                cki = Console.ReadKey(true);
               }
            while (cki.Key != ConsoleKey.Enter);
        }
    }
}
