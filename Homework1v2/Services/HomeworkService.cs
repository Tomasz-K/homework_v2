﻿using Homework1v2.Models;
using System.Collections.Generic;

namespace Homework1v2.Services
{
    public class HomeworkService
    {
        private ConsoleWriter _consoleWriter = new ConsoleWriter();

        private ValidateInput _validateUserInput = new ValidateInput();

        public Homework AddHomework(List<Student> studentsList)
        {
            Homework homework = new Homework();

            _consoleWriter.WriteMessage("Podaj liczbę punktów do zdobycia: ");
            homework.MaxPoints = _validateUserInput.CheckCount();

            homework.ResultsList = new List<HomeworkResult>();

            foreach (var student in studentsList)
            {
                HomeworkResult homeworkResult = new HomeworkResult();
                homeworkResult.Student = student;

                _consoleWriter.WriteMessage($"Ile punktów zdobył {student.FirstName} {student.LastName}? ");
                double studentResult = _validateUserInput.CheckStudentResult();

                while (studentResult > homework.MaxPoints)
                {
                    _consoleWriter.WriteMessage("Liczba punktów uzyskanych większa niż maksymalna!\n");
                    studentResult = _validateUserInput.CheckStudentResult();
                }
                homeworkResult.StudentResult = studentResult;

                homework.ResultsList.Add(homeworkResult);
            }

            return homework;

        }
    }
}
